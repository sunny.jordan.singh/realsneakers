import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemVueComponent } from './item-vue.component';

describe('ItemVueComponent', () => {
  let component: ItemVueComponent;
  let fixture: ComponentFixture<ItemVueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemVueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemVueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
