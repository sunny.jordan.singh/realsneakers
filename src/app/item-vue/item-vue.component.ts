import {Component, Input, OnInit} from '@angular/core';
import {ProductServ} from "../service/product-service.service";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {Product} from "../model/product";

@Component({
  selector: 'app-item-vue',
  templateUrl: './item-vue.component.html',
  styleUrls: ['./item-vue.component.css']
})

export class ItemVueComponent implements OnInit {
  @Input() valueIndex=0;
  // @ts-ignore
  product: Observable<Product>;
  // @ts-ignore
  title: Observable<string>;
  image = '';
  price = '';
  resume = '';


  constructor(private productServ: ProductServ) {

  }

  ngOnInit(): void {
    this.productServ.getProductsFromMongoDBByIndex(this.valueIndex).pipe(
      map(s => console.log('TEST >>>>>>>>> ' + s))
    );

    this.product = this.productServ.getProductsFromMongoDBByIndex(this.valueIndex).pipe(
      map(s => s)
    );
  }

}
