import { Component, OnInit } from '@angular/core';
import {Product} from "../model/product";
import {ProductServ} from "../service/product-service.service";

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  product: Product[] = [];


  constructor(private productService: ProductServ) { }

  ngOnInit(): void {
    this.product = this.productService.productMockList;
  }

  loadCarsLazy($event: any) {
    
  }
}
