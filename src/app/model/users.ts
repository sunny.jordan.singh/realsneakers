import {Product} from "./product";

export type STATUS  = 'user' | 'admin'

export interface Users {
  status : STATUS;
  firstName : string;
  lastName : string;
  email : string;
  phone : string;
  address : string;
  carts : Product[];
}
