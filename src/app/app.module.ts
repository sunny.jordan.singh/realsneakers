import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MenubarModule} from 'primeng/menubar';
import {HeaderComponent} from "./acceuil/fragment/header/header.component";
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {AccordionModule} from 'primeng/accordion'
import {MenuModule} from 'primeng/menu';
import {CarouselComponent} from "./acceuil/fragment/carousel/carousel.component";
import {CarouselModule} from "primeng/carousel";
import {ToastModule} from 'primeng/toast';
import {HttpClientModule} from "@angular/common/http";
import {ItemCardComponent} from './acceuil/fragment/item-card/item-card.component';
import {CardModule} from 'primeng/card';
import {ItemVueComponent} from './item-vue/item-vue.component';
import {RouterModule} from "@angular/router";
import {HomeComponent} from './acceuil/home/home.component';
import {IdentificationComponent} from "./identification/identification/identification.component";
import {FormsModule} from "@angular/forms";
import {InputMaskModule} from "primeng/inputmask";
import {InputNumberModule} from "primeng/inputnumber";
import {PasswordModule} from 'primeng/password';
import { SubscribeComponent } from './identification/subscribe/subscribe.component';
import {OrderListModule} from 'primeng/orderlist';
import { BasketComponent } from './basket/basket.component';
import {VirtualScrollerModule} from 'primeng/virtualscroller';
import { AdminFormComponent } from './admin-form/admin-form.component';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {KeyFilterModule} from "primeng/keyfilter";
import {RippleModule} from 'primeng/ripple';
import {FileUploadModule} from "primeng/fileupload";
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CarouselComponent,
    ItemCardComponent,
    ItemVueComponent,
    HomeComponent,
    IdentificationComponent,
    SubscribeComponent,
    BasketComponent,
    AdminFormComponent
  ],
    imports: [
        RouterModule,
        BrowserAnimationsModule,
        AccordionModule,
        BrowserModule,
        AppRoutingModule,
        MenubarModule,
        InputTextModule,
        ButtonModule,
        MenuModule,
        CarouselModule,
        ToastModule,
        HttpClientModule,
        CardModule,
        FormsModule,
        InputMaskModule,
        InputNumberModule,
        PasswordModule,
        OrderListModule,
        VirtualScrollerModule,
        InputTextareaModule,
        KeyFilterModule,
        RippleModule,
        FileUploadModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
