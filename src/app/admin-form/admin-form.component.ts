import { Component, OnInit } from '@angular/core';
import {ProductServ} from "../service/product-service.service";
import {Observable} from "rxjs";
import {FileUploadModule} from 'primeng/fileupload';
import {HttpClientModule} from '@angular/common/http';
import {MessageService} from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-admin-form',
  templateUrl: './admin-form.component.html',
  styleUrls: ['./admin-form.component.css'],
  providers: [MessageService]
})
export class AdminFormComponent implements OnInit {
  // @ts-ignore
  isSend: Observable<any>;
  regex: string = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)";
  // @ts-ignore
  id: string;
  // @ts-ignore
  code: string;
  // @ts-ignore
  name: string;
  // @ts-ignore
  description: string;
  // @ts-ignore
  image: string;
  // @ts-ignore
  price: string;
  // @ts-ignore
  qte: string;

  constructor(private productService: ProductServ, private messageService: MessageService, private primengConfig: PrimeNGConfig) {}

  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

  sendData(id: string, code: string, name: string, description: string, image: string, price: string, quantity: string) {
    let formatName = name.split(" ").join("_");
    let formatImage = encodeURIComponent(image);
    let formatDescription = encodeURIComponent(description.split(" ").join("_"));
    console.log('format ' + formatDescription);
    this.isSend = this.productService.sendShoesToMongoDB(id, code, formatName, formatDescription, formatImage, price, quantity);
    console.log('bool' +  this.isSend);
    this.showSuccess();
  }

  showSuccess() {
    this.messageService.add({severity:'success', summary: 'Success', detail: 'Message Content'});
  }
}
