import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ItemVueComponent} from "./item-vue/item-vue.component";
import {HomeComponent} from "./acceuil/home/home.component";
import {IdentificationComponent} from "./identification/identification/identification.component";
import {SubscribeComponent} from "./identification/subscribe/subscribe.component";
import {BasketComponent} from "./basket/basket.component";
import {ItemCardComponent} from "./acceuil/fragment/item-card/item-card.component";
import {AdminFormComponent} from "./admin-form/admin-form.component";

const routes: Routes = [
  {path: '', redirectTo: 'index', pathMatch: 'full'},
  {path: 'index', component: HomeComponent},
  {path: 'vue', component: ItemVueComponent},
  {path: 'signin', component: IdentificationComponent},
  {path: 'sub', component: SubscribeComponent},
  {path: 'basket', component: BasketComponent},
  {path: 'item', component: ItemCardComponent},
  {path: 'admin', component: AdminFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
