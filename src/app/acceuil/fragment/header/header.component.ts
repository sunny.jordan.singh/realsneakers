import {Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {Router} from "@angular/router";
import {IdentificationService} from "../../../service/identification.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  items: MenuItem[] = [];

  constructor(private identService : IdentificationService){

  }

  ngOnInit() {
    this.items = [
      {
        label:'HOME',
        icon:'pi pi-home'
      },
      {
        label:'NIKE',
        icon:'pi pi-star-o'
      },
      {
        label:'ADIDAS',
        icon:'pi pi-star-o',
      },
      {
        label:'JORDAN',
        icon:'pi pi-star-o'
      },
      {
        label:'CONVERSE',
        icon:'pi pi-star-o'
      },
      {
        label:'SIGN IN',
        icon:'pi pi-user-plus'
      },
      {
        label:'CONNEXION',
        icon:'pi pi-user'
      },
      {
        label:'USER',
        icon:'pi pi-user-edit'
      },
      {
        label:'PANIER',
        icon:'pi pi-shopping-cart'
      },
      {
        label:'ADMIN',
        icon:'pi pi-users'
      }
    ];
  }


  selectMenu($event: any) {
    let menu = $event.path[0].innerText;
    switch(menu){
      case 'HOME' :
        this.identService.goToHome();
        break;
      case 'CONNEXION' :
        this.identService.goToConnexion();
        break;
      case 'SIGN IN' :
        this.identService.goToSub();
        break;
      case 'NIKE' :
      case 'ADIDAS' :
        // this.identService.goToHomeInoutItem('ADIDAS');
        break;
      case 'JORDAN' :
      case 'CONVERSE' :
      case 'PANIER' :
        this.identService.goToBasket();
        break;
      case 'ADMIN' :
        this.identService.goToAdmin()
        break;
    }

  }
}
