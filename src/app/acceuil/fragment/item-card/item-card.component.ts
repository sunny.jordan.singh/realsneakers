import {Component, OnInit} from '@angular/core';
import {ProductServ} from "../../../service/product-service.service";
import {map} from "rxjs/operators";
import {Observable, pipe, Subscription} from "rxjs";
import {Product} from "../../../model/product";

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.css']
})
export class ItemCardComponent implements OnInit {

  // @ts-ignore
  item: Observable<Product[]>;
  private _productServ: ProductServ;
  clickVue: boolean = false;
  indexVue = -1;

  constructor(productServ: ProductServ) {
    this._productServ = productServ;
  }

  ngOnInit(): void {
    this.item =this._productServ.getProductsFromMongoDB().pipe(
      map(s => s.map(s => s))
    );
  }

  isSelectVue(index: number): void {
    this.clickVue = true;
    this.indexVue = index;
  }

  isNotSelectVue(): void {
    this.clickVue = false;
    this.indexVue = -1;
  }

}
