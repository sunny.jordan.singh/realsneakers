import {Component, OnInit} from '@angular/core';
import {Product} from "../../../model/product";
import {ProductServ} from "../../../service/product-service.service";

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  products: Product[] = [];

  responsiveOptions: any;

  constructor(private productService: ProductServ) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }


  ngOnInit(): void {
    this.products = this.productService.productMockList;
  }

}
