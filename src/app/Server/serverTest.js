"use strict";
const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
const url = "mongodb://localhost:27017/real_sneakers/";
const express = require("express");
const app = express();
app.listen(8888);
const cors = require("cors");
app.use(cors());




function searchShoes(db, param, callback) {
  let cursor = db.collection("Products").find(param).toArray(
      (err, doc) => {
        if (err) {
          callback(err, []);
        } else if (doc !== undefined) {
          // console.log('...........');
          // console.log(JSON.stringify(doc));
          // console.log('-------------------------------------------');
          callback(param["message"], doc);
        } else {
          callback(param["message"], []);
        }
      }
  );
  // console.log("array = " + cursor);
}

MongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
  let db = client.db("real_sneakers");
  assert.equal(null, err);

  app.get("/Products", (req, res) => {
    searchShoes(db, {}, (step, result) => {
      // console.log("step  = " + step);
      let prod = [];
      for (let it in result) {
        prod.push(result[it]);
        // console.log("it  = " +JSON.stringify(result[it]));
      }
      const json = JSON.stringify(prod);
      res.setHeader("Content-type", "application/json; charset=UTF-8");
      res.end(json);
    });
  });


  app.get("/Products/:index", (req, res) => {
    searchShoes(db, {}, (step, result) => {
      // console.log("step  = " + step);
      let prod;
      let i = 0;
      // console.log('>>>>------------------->' +req.params.index);
      for (let it in result) {
        // console.log('>>>>' + i+'\n');
        if (req.params.index == i){
          prod = result[it];
          // console.log('>>>>' + result[it]);
        }
        i++;
        // console.log("it  = " +JSON.stringify(result[it]));
      }
      const json = JSON.stringify(prod);
      res.setHeader("Content-type", "application/json; charset=UTF-8");
      res.end(json);
    });
  });



  app.get("/Products/admin/insert/:id/:code/:name/:description/:price/:quantity/:image", (req, res) => {
    console.log("-->add Shoes  -->");
    let imageFormatUrl = decodeURIComponent(req.params.image);
    let formatDescription = decodeURIComponent(req.params.description);

    let formatId = req.params.id.split("_").join(" ");
    let formatCode = req.params.code.split("_").join(" ");
    let formatName = req.params.name.split("_").join(" ");
    let formatPrice = req.params.price.split("_").join(" ");
    let formatQte = req.params.quantity.split("_").join(" ");
    db.collection("Products").insertOne(
        {
          "id": formatId,
          "code": formatCode,
          "name": formatName,
          "description": formatDescription,
          "image":imageFormatUrl,
          "price": formatPrice,
          "category": "shoes",
          "quantity":formatQte,
          "inventoryStatus": "STOCK",
          "rating": 5
        }
    );
    res.setHeader("Content-type", "application/json; charset=UTF-8");

    const json = JSON.stringify({"result":true});
    res.end(json);

  });


  app.get("/Products/user/insert/:firstName/:lastName/:email/:phone/:address/:password", (req, res) => {
    console.log("-->add User");
    let fname= decodeURIComponent(req.params.firstName.split("_").join(" "));
    let lname= decodeURIComponent(req.params.lastName.split("_").join(" "));
    let phoneEnc= decodeURIComponent(req.params.phone.split("_").join(" "));
    let emailEnc= decodeURIComponent(req.params.email.split("_").join(" "));
    let addrEnc= decodeURIComponent(req.params.address.split("_").join(" "));
    let passwordEnc= decodeURIComponent(req.params.password.split("_").join(" "));
    consolelog(fname);
    consolelog(lname);
    consolelog(phoneEnc);
    consolelog(emailEnc);
    consolelog(addrEnc);
    consolelog(passwordEnc);

    db.collection("Users").insertOne(
        {
          "firstName":fname ,
          "lastName":lname ,
          "email": emailEnc,
          "phone":phoneEnc,
          "address": addrEnc,
          "password":passwordEnc ,
        }
    );
    res.setHeader("Content-type", "application/json; charset=UTF-8");

    const json = JSON.stringify({"result":true});
    res.end(json);

  });








})
