import {Injectable} from '@angular/core';
import {Product} from "../model/product";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Users} from "../model/users";

@Injectable({
  providedIn: 'root'
})
export class ProductServ {


  url: string = "http://localhost:8888/products";
  urlIndex: string = "http://localhost:8888/products/";
  urlAdd: string = "http://localhost:8888/products/admin/insert/";
  urlUSER :string = "http://localhost:8888/products/user/insert/";

  status: string[] = ['OUTOFSTOCK', 'INSTOCK', 'LOWSTOCK'];

  productMockList: any = [
    {
      "id": "1000",
      "code": "f230fh0g3",
      "name": "Jordan travis Scoot",
      "description": "Product Description",
      "image": "https://limitedresell.com/1570-full_default/air-jordan-1-retro-high-travis-scott-cactus-jack.jpg",
      "price": 65,
      "category": "Accessories",
      "quantity": 24,
      "inventoryStatus": "INSTOCK",
      "rating": 5
    },
    {
      "id": "1001",
      "code": "nvklal433",
      "name": "Jordan 1 chateau - rouge",
      "description": "Product Description",
      "image": "https://cdn.shopify.com/s/files/1/0050/3326/3173/products/Nike-Air-Jordan-1-Mid-Fearless-Maison-Chateau-Rouge-CU2803-200-Sneakers-Heat-1_1024x.png?v=1575907852",
      "price": 72,
      "category": "Accessories",
      "quantity": 61,
      "inventoryStatus": "INSTOCK",
      "rating": 4
    },
    {
      "id": "1002",
      "code": "zz21cz3c1",
      "name": "Jordan 1 Dior",
      "description": "test",
      "image": "http://cdn.shopify.com/s/files/1/0496/4325/8009/products/baskets-air-jordan-1-high-dior-air-jordan-kikikickz-111359_1200x1200.jpg?v=1612371089",
      "price": 79,
      "category": "Fitness",
      "quantity": 2,
      "inventoryStatus": "LOWSTOCK",
      "rating": 3
    },
    {
      "id": "1003",
      "code": "244wgerg2",
      "name": "Nike Sacai Vaporwaffle",
      "description": "Product Description",
      "image": "https://cdn.shopify.com/s/files/1/2358/2817/products/wethenew-sneakers-france-sacai-vaporwaffle-green-yellow-1.png?v=1608572677",
      "price": 29,
      "category": "Clothing",
      "quantity": 25,
      "inventoryStatus": "INSTOCK",
      "rating": 5
    },
    {
      "id": "1004",
      "code": "h456wer53",
      "name": "Nike Dunk low Laser Orange",
      "description": "Product Description",
      "image": "https://cdn.shopify.com/s/files/1/0022/0879/2694/products/nike-dunk-low-laser-orange-SneakersFromFrance-0_800x_faa506b9-ba75-4b2b-97ca-a90bf4d3f6ec_2000x.png?v=1622622971\n",
      "price": 15,
      "category": "Accessories",
      "quantity": 73,
      "inventoryStatus": "INSTOCK",
      "rating": 4
    },
    {
      "id": "1005",
      "code": "av2231fwg",
      "name": "Nike Dunk low Green glow",
      "description": "Product Description",
      "image": "https://cdn.shopify.com/s/files/1/0022/0879/2694/products/nike-dunk-low-green-glow-SneakersFromFrance-0_800x_2a90962f-3ddc-4a91-8279-832f2f1f59cd_2000x.png?v=1618738254",
      "price": 120,
      "category": "Accessories",
      "quantity": 0,
      "inventoryStatus": "OUTOFSTOCK",
      "rating": 4
    },
    {
      "id": "1006",
      "code": "bib36pfvm",
      "name": "Adidas Yeezy 380 covelite",
      "description": "Product Description",
      "image": "https://cdn.shopify.com/s/files/1/2358/2817/products/Wethenew-Sneakers-France-Adidas-Yeezy-Boost-350-V2-Linen-FY5158-1_800x.png?v=1588149301",
      "price": 32,
      "category": "Accessories",
      "quantity": 5,
      "inventoryStatus": "LOWSTOCK",
      "rating": 3
    },
    {
      "id": "1007",
      "code": "mbvjkgip5",
      "name": "Adidas Yeezy 700",
      "description": "Product Description",
      "image": "https://cdn.shopify.com/s/files/1/2358/2817/products/Wethenew-Sneakers-France-Adidas-Yeezy-700-Wave-Runner-Solid-Grey-1_800x.png?v=1569141266",
      "price": 34,
      "category": "Accessories",
      "quantity": 23,
      "inventoryStatus": "INSTOCK",
      "rating": 5
    },
    {
      "id": "1008",
      "code": "vbb124btr",
      "name": "Yeezy 500 Soft Vision",
      "description": "Product Description",
      "image": "https://cdn.shopify.com/s/files/1/2358/2817/products/Wethenew-Sneakers-France-Adidas-Yeezy-500-Soft-Vision-1_800x.png?v=1572689933",
      "price": 99,
      "category": "Electronics",
      "quantity": 2,
      "inventoryStatus": "LOWSTOCK",
      "rating": 4
    }
  ];

  constructor(private http: HttpClient) {
  }


  getProducts(): any {
    return this.http.get<any>("../../../assets/data.json")
      .toPromise()
      .then(res => <Product[]>res.data)
      .then(data => {
        return data;
      });
  }

  getProductsFromMongoDB(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url);
  }

  getProductsFromMongoDBByIndex(index: number): Observable<Product> {
    let req = this.urlIndex + index.toString()
    return this.http.get<Product>(req);
  }

  sendShoesToMongoDB(id: string, code: string, name: string, description: string, image: string, price: string, quantity: string) :Observable<any>{
    return this.http.post<Product>(this.urlAdd,+ `${id}/${code}/${name}/${description}/${price}/${quantity}/${image}`);
  }
  sendUserToMongoDB(firstName: string, lastName: string, email: string, phone: string, address: string, password: string) :Observable<any>{
    return this.http.get<Users>(this.urlUSER + `${firstName}/${lastName}/${email}/${phone}/${address}/${password}`);
  }

//  todo implement the search for all mark shoes


}
