import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {Product} from "../model/product";
import {ProductServ} from "./product-service.service";

@Injectable({
  providedIn: 'root'
})
export class IdentificationService {

  products: Product[] = [];
  constructor(private productService: ProductServ, private router: Router) {}

  ngOnInit(): void {
    this.products = this.productService.productMockList;
  }


  goToConnexion() {
    this.router.navigate(['signin']);
  }

  goToSub() {
    this.router.navigate(['sub']);
  }

  goToHome() {
    this.router.navigate(['index',]);
  }
  goToHomeInoutItem(label : string) {
    this.router.navigate(['/item','ADIDAS']);
  }

  goToBasket() {
    this.router.navigate(['basket']);
  }

  goToAdmin(){
    this.router.navigate(['admin']);
  }

  searchAdidas() : Product[]{
    // return this.products.filter(item => item.name.includes('Adidas'));
    return  [
      {
        "id": "1000",
        "code": "f230fh0g3",
        "name": "Jordan travis Scoot",
        "description": "Product Description",
        "image": "https://limitedresell.com/1570-full_default/air-jordan-1-retro-high-travis-scott-cactus-jack.jpg",
        "price": 65,
        "category": "Accessories",
        "quantity": 24,
        "inventoryStatus": "INSTOCK",
        "rating": 5
      }];
  }



}
