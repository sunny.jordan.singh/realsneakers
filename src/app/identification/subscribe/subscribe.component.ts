import {Component, OnInit} from '@angular/core';
import {ProductServ} from "../../service/product-service.service";

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  //@ts-ignore
  firstName: string;
  //@ts-ignore
  lastName: string;
  //@ts-ignore
  email: string;
  //@ts-ignore
  phone: string;
  //@ts-ignore
  address: string;
  //@ts-ignore
  password: string;
  //@ts-ignore
  verifPassword: string;

  constructor(private productService: ProductServ) {
  }

  ngOnInit(): void {
  }

  sendData(firstName: string, lastName: string, email: string, phone: string, address: string, password: string){
    if (this.password == this.verifPassword) {
      console.log("password verif ");
      let fname= encodeURIComponent(firstName.split(" ").join("_"));
      let lname= encodeURIComponent(lastName.split(" ").join("_"));
      let phoneEnc= encodeURIComponent(phone.split(" ").join("_"));
      let emailEnc= encodeURIComponent(email.split(" ").join("_"));
      let addrEnc= encodeURIComponent(address.split(" ").join("_"));
      let passwordEnc= encodeURIComponent(password.split(" ").join("_"));
      this.productService.sendUserToMongoDB(fname, lname, emailEnc, phoneEnc, addrEnc, passwordEnc);
    }
    console.log("nope ");

  }

}

